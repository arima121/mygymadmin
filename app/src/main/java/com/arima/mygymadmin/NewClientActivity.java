package com.arima.mygymadmin;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.widget.EditText;

import com.arima.mygymadmin.storage.FirestoreManager;

public class NewClientActivity extends AppCompatActivity {
    private NewClientActivity thisActivity;

    private FirestoreManager dbManager;

    private EditText editName;
    private EditText editDni;
    private EditText editPhone;
    private EditText editEmail;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_new_client);

        thisActivity = this;

        dbManager = new FirestoreManager();

        createViewItems();
    }

    public void createViewItems() {
        editName = findViewById(R.id.name);
        editDni = findViewById(R.id.dni);
        editPhone = findViewById(R.id.phone);
        editEmail = findViewById(R.id.email);
    }
}
