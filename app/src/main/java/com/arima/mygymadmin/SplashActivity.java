package com.arima.mygymadmin;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;

import com.arima.mygymadmin.auth.FirebaseAuthManager;
import com.arima.mygymadmin.model.User;
import com.arima.mygymadmin.storage.FirestoreManager;
import com.arima.mygymadmin.storage.SharedPreferencesManager;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.QueryDocumentSnapshot;

public class SplashActivity extends AppCompatActivity {
    private SplashActivity thisActivity;

    private FirebaseAuthManager authManager;
    private FirestoreManager dbManager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        thisActivity = this;

        authManager = new FirebaseAuthManager();
        dbManager = new FirestoreManager();

        checkUserAndRedirect();
    }

    private void checkUserAndRedirect() {
        if(!authManager.isLogin()) {
            redirectToLogin();
            return;
        }
        User currentUser = authManager.getUser();
        dbManager.getDocument(FirestoreManager.FS_COLLECTION_USERS, currentUser.getUid(), response -> {
            if(!response.isSuccessful()) {
                authManager.logout();
                redirectToLogin();
                return;
            }

            if(response.getResult() == null) {
                authManager.logout();
                redirectToLogin();
                return;
            }

            User responseUser = response.getResult().toObject(User.class);

            currentUser.setType(responseUser.getType());
            currentUser.setDefaultTarget(responseUser.getDefaultTarget());
            currentUser.setTargets(responseUser.getTargets());

            if(!currentUser.getType().equals(User.USER_TYPE_ADMIN)) {
                authManager.logout();
                redirectToLogin();
                return;
            }

            redirectToClientList();
        });
    }


    private void redirectToLogin() {
        Intent intent = new Intent(thisActivity, LoginActivity.class);
        startActivity(intent);
        finish();
    }

    private void redirectToClientList() {
        Intent intent = new Intent(thisActivity, ClientListActivity.class);
        startActivity(intent);
        finish();
    }
}
