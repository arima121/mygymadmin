package com.arima.mygymadmin.model;

import com.google.firebase.firestore.Exclude;
import com.google.firebase.firestore.PropertyName;

import java.util.ArrayList;
import java.util.Collection;

public class User {
    private String uid;
    private String email;
    private String type;
    private String defaultTarget;
    private ArrayList<String> targets;


    public static final String USER_TYPE_ADMIN = "admin";

    public void User() {
        targets = new ArrayList<>();
    }

    @Exclude
    public String getUid() {
        return uid;
    }

    @Exclude
    public void setUid(String uid) {
        this.uid = uid;
    }

    @PropertyName("email")
    public String getEmail() {
        return email;
    }

    @PropertyName("email")
    public void setEmail(String email) {
        this.email = email;
    }

    @PropertyName("type")
    public String getType() {
        return type;
    }

    @PropertyName("type")
    public void setType(String type) {
        this.type = type;
    }

    @PropertyName("defaultTarget")
    public String getDefaultTarget() {
        return defaultTarget;
    }

    @PropertyName("defaultTarget")
    public void setDefaultTarget(String defaultTarget) {
        this.defaultTarget = defaultTarget;
    }

    @PropertyName("targets")
    public ArrayList<String> getTargets() {
        return targets;
    }

    @PropertyName("targets")
    public void setTargets(ArrayList<String> targets) {
        this.targets = targets;
    }

    public void addTarget(String target) {
        this.targets.add(target);
    }

    public void addTargets(Collection targets) {
        this.targets.addAll(targets);
    }
}
