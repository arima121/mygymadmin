package com.arima.mygymadmin.storage;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.QuerySnapshot;

public class FirestoreManager {
    //Firestore collections
    public static final String FS_COLLECTION_USERS = "users";

    //Firestore documents

    //Firestore fields

    private FirebaseFirestore db;

    public FirestoreManager() {
        this.db = FirebaseFirestore.getInstance();
    }

    public void getCollection(String collectionPath, OnCompleteListener<QuerySnapshot> responseHandler){
        db.collection(collectionPath)
                .get()
                .addOnCompleteListener(responseHandler);
    }

    public void getCollectionWhereEqualsTo(String collectionPath, String field, String value, OnCompleteListener<QuerySnapshot> responseHandler){
        db.collection(collectionPath)
                .whereEqualTo(field, value)
                .get()
                .addOnCompleteListener(responseHandler);
    }

    public void getDocument(String collectionPath, String documentKey, OnCompleteListener<DocumentSnapshot> responseHandler) {
        db.collection(collectionPath)
                .document(documentKey)
                .get()
                .addOnCompleteListener(responseHandler);
    }

    public void setValue(String collectionPath, String documentKey, Object objectToSave, OnSuccessListener<Void> successHandler, OnFailureListener failureHandler){
        db.collection(collectionPath)
                .document(documentKey)
                .set(objectToSave)
                .addOnSuccessListener(successHandler)
                .addOnFailureListener(failureHandler);
    }

    public void addValue(String collectionPath, Object objectToSave, OnSuccessListener<DocumentReference> successHandler, OnFailureListener failureHandler){
        db.collection(collectionPath)
                .add(objectToSave)
                .addOnSuccessListener(successHandler)
                .addOnFailureListener(failureHandler);
    }
}
