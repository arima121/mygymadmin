package com.arima.mygymadmin;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;

import com.arima.mygymadmin.adapters.ClientListAdapter;
import com.arima.mygymadmin.auth.FirebaseAuthManager;
import com.arima.mygymadmin.storage.FirestoreManager;
import com.arima.mygymadmin.storage.SharedPreferencesManager;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.QueryDocumentSnapshot;

import java.util.ArrayList;

public class ClientListActivity extends AppCompatActivity {
    private ClientListActivity thisActivity;

    private FirebaseAuthManager authManager;
    private FirestoreManager dbManager;

    private RecyclerView clientListView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_client_list);

        thisActivity = this;

        authManager = new FirebaseAuthManager();
        dbManager = new FirestoreManager();

        createViewItems();
    }

    public void createViewItems() {
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        clientListView = findViewById(R.id.client_list);
        clientListView.setHasFixedSize(true);

        final ClientListAdapter clientListAdapter = new ClientListAdapter();

        clientListView.setAdapter(clientListAdapter);
        clientListView.setLayoutManager(new LinearLayoutManager(thisActivity,LinearLayoutManager.VERTICAL,true));
    }

    @Override
    public void onResume() {
        super.onResume();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.new_client, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.logout:
                this.authManager.logout();
                Intent intent = new Intent(thisActivity, LoginActivity.class);
                startActivity(intent);
                finish();
                return true;

            default:
                return super.onOptionsItemSelected(item);

        }
    }

    public void goToNewClient(View view) {
        Intent intent = new Intent(thisActivity, NewClientActivity.class);
        startActivity(intent);
    }
}
