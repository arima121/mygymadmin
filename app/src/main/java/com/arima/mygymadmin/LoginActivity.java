package com.arima.mygymadmin;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;

import com.arima.mygymadmin.auth.FirebaseAuthManager;
import com.arima.mygymadmin.model.User;
import com.arima.mygymadmin.storage.FirestoreManager;
import com.google.android.material.snackbar.Snackbar;
import com.google.firebase.firestore.DocumentSnapshot;

public class LoginActivity extends AppCompatActivity {
    private LoginActivity thisActivity;

    private FirebaseAuthManager authManager;
    private FirestoreManager dbManager;

    private EditText email;
    private EditText password;
    private Button login;
    private ProgressBar loadingPB;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        thisActivity = this;

        authManager = new FirebaseAuthManager();
        dbManager = new FirestoreManager();

        createViewItems();
    }

    public void createViewItems() {
        email = findViewById(R.id.login_email);
        password = findViewById(R.id.login_password);
        login = findViewById(R.id.login_ok);
        loadingPB = findViewById(R.id.loading);
    }

    public void login(View view) {
        setLoadingState(true);

        String textEmail = email.getText().toString();
        String textPassword = password.getText().toString();
        if(textEmail.isEmpty() || textPassword.isEmpty()){
            Snackbar.make(view, R.string.login_no_values_error, Snackbar.LENGTH_LONG).show();
            setLoadingState(false);
            return;
        }

        authManager.login(thisActivity, email.getText().toString(), password.getText().toString(), task -> {
            if(!task.isSuccessful()) {
                authManager.logout();
                Snackbar.make(view, R.string.login_error, Snackbar.LENGTH_LONG).show();
                setLoadingState(false);
                return;
            }

            validateAdmin();
        });
    }

    public void validateAdmin() {
        User currentUser = authManager.getUser();
        dbManager.getDocument(FirestoreManager.FS_COLLECTION_USERS, currentUser.getUid(), response -> {
            if(!response.isSuccessful()) {
                authManager.logout();
                Snackbar.make(email, R.string.login_no_admin_error, Snackbar.LENGTH_LONG).show();
                setLoadingState(false);
                return;
            }

            if(response.getResult() == null) {
                authManager.logout();
                Snackbar.make(email, R.string.login_no_admin_error, Snackbar.LENGTH_LONG).show();
                setLoadingState(false);
                return;
            }

            User responseUser = response.getResult().toObject(User.class);

            currentUser.setType(responseUser.getType());
            currentUser.setDefaultTarget(responseUser.getDefaultTarget());
            currentUser.setTargets(responseUser.getTargets());

            if(!currentUser.getType().equals(User.USER_TYPE_ADMIN)) {
                authManager.logout();
                Snackbar.make(email, R.string.login_no_admin_error, Snackbar.LENGTH_LONG).show();
                setLoadingState(false);
                return;
            }

            setLoadingState(false);
            redirectToClientList();
        });
    }

    public void setLoadingState(boolean loading) {
        email.setEnabled(!loading);
        password.setEnabled(!loading);
        login.setClickable(!loading);

        loadingPB.setVisibility(loading ? View.VISIBLE : View.GONE);
    }

    public void redirectToClientList(){
        Intent intent = new Intent(thisActivity, ClientListActivity.class);
        startActivity(intent);
        finish();
    }
}
